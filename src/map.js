// запрос на получение json регионов
window.store = {};

window.onload = function() {
  const regionsData = fetch("../static_info/regions.json").then(function(
    regions
  ) {
    return regions.json();
  });

  const paintData = fetch("./with-regions.json").then(function(paint) {
    return paint.json();
  });

  Promise.all([regionsData, paintData]).then(([regions, data]) => {
    window.store.regions = regions;
    let width = window.innerWidth- document.body.clientWidth*0.1;
    let height = document.body.clientHeight- document.body.clientHeight*0.4;

    setMap(regions, data, width, height);

    // window.onresize = () => {setMap(regions, data, width, 0);};
  });
};

function setMap(regions, data, width, height) {
  var ev = new Event('bii');

  new RussianMap(
    {
      viewPort: data.viewPort,
      mapId: "russian-map",
       width:  width,
       height: height,
      // дефолтовые атрибуты для контуров регионов
      defaultAttr: {
        fill: "#d8d8d8", // цвет которым закрашивать
        stroke: "#ffffff", // цвет границы
        "stroke-width": 1, // ширина границы
        "stroke-linejoin": "round" // скруглять углы
      },
      mouseMoveAttr: {
        fill: "#25669e"
      },
      onMouseMove: function(event) {
        console.log(
          "mouse on " +
            this.region.name +
            " (ident: " +
            this.region.ident +
            ")",
          this.region
        );
        app["region"] = this.region.name;
      },
      onMouseOut: function(event) {
        console.log(
          "out on " + this.region.name + " (ident: " + this.region.ident + ")"
        );

        app["region"] = "";
        
      },
      onMouseClick: function(event) {
        // document.getElementById("app").setAttribute("style", "visibility:hidden");

        console.log("clicked on " + this.region.name);
        window.regId = this.region.ident;
        document.dispatchEvent(ev);
        document.getElementById("vueapp").style.display = "block";
      }
    },
    data.regions,
    regions
  );
}

const initapp = {};
const styler = {};

const app = new Proxy(initapp, {
  set(oTarget, sKey, vValue) {
    console.log("set");
    oTarget[sKey] = vValue;
    const node = document.getElementById(sKey);

    if (node) {
      node.innerHTML = oTarget[sKey];
      if (styler[sKey]) {
        styler[sKey](node, vValue);
      }
    }
    return oTarget[sKey];
  }
});

styler["region"] = (node, vValue) => {
  if (!vValue) {
    node.setAttribute("style", "visibility:hidden");
  } else {
    node.setAttribute("style", "visibility:visible");
  }
};

console.log(app);

app["region"] = "Московская область";



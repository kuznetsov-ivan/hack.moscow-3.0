     Vue.component('yellow-block', {
        data: function () {
          return {
            count: 0
          }
        },
        computed: {
          styleObj() {
            if (this.rate) {
              return {
              'background-color': 'rgb(' + hsv2rgb(92, 0, Math.floor((10 - this.rate) * 10 )).join(',') + ')',
              'color': 'rgb(' + hsv2rgb(92, 0,this.rate < 5? 0 : 100).join(',') + ')'
            }
            }else {
              return '';
            }
          }
        },
        props: ['ttt', 'rate','position'],
        template: `<div class='yellow-wrap'>
          <div class="dot" v-if="rate" v-bind:style="styleObj">{{rate ? rate.toFixed(1): ''}}</div>
          <div class="yellow">{{ttt}}<br>
          <span v-if="position" class="pos">{{position}}</span>
          </div>
        </div>`
      });

       Vue.component('yellow-block-aff', {
        data: function () {
          return {
            count: 0,
            officialsInfo: {},
          }
        },
        computed: {
          styleObj() {
            if (this.officialsInfo.rating) {
              return {
              'background-color': 'rgb(' + hsv2rgb(92, 0, Math.floor((10 - this.officialsInfo.rating) * 10 )).join(',') + ')',
              'color': 'rgb(' + hsv2rgb(92, 0, this.officialsInfo.rating  < 5? 0 : 100).join(',') + ')'
            }
            }else {
              return '';
            }
          }
        },
        methods: {
          getit() {
            var ev1 = new Event('person');
            window.store.person = this.officialsInfo;
            document.dispatchEvent(ev1);
          },
        },
        mounted(){
          fetch("../static_info/officials/" + Math.floor(this.ttt/100) + ".json").then((officials) =>{
              officials.json().then((data) => {
                // console.log(data);
                this.officialsInfo = data[this.ttt];
              });
          });
        },
        filters: {
          capitalize: function (value) {
            if (!value) return ''
            value = value.toString()
            return value.split(' ').map(str=> str.charAt(0).toUpperCase() + str.slice(1)).join(' ')
          }
        },
        props: ['ttt', 'rate', 'position'],
        template: `<div v-if="officialsInfo" class='yellow-wrap' @click="getit()">
          <div class="dot" v-if="officialsInfo.rating" v-bind:style="styleObj">{{officialsInfo.rating ? officialsInfo.rating.toFixed(1): '' }}</div>
          <div class="yellow d-block">{{officialsInfo.person_name|capitalize}}<br>
            <span v-if="officialsInfo.last_position" class="pos">{{officialsInfo.last_position}}</span>
          </div>
        </div>`
      });
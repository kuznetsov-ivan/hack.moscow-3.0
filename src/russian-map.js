/**
 * Рендер Российской карты.
 *
 * Перед подключением этой библиотеки должна быть подключена библиотека RaphelJS:
 *
 * @see http://dmitrybaranovskiy.github.io/raphael/
 *
 * Состав опций:
 * - mapId - идентификатор элемента, в котором будет срендерена карта
 * - width - ширина карты
 * - height - высота карты
 * - defaultAttr - дефолтовые атрибуты полигона
 * - mouseMoveAttr(region) - атрибуты для полигона при наведении мышки
 * - onMouseMove - событие при наведении мышки на регион
 * - onMouseOut - событие при убирании мышки с региона
 * - onMouseClick - событие при клике
 * - viewPort - смещение координат в SVG
 * - viewSize - видимые границы в SVG
 *
 * @param {Object} options Опции
 * @param {Array} regions Регионы, каждый элемент состоит из:
 *  ident - идентификатора,
 *  name - названия,
 *  attr - необязательное, атрибуты, которые будут перекрывать атрибуты по умолчанию,
 *  moveAttr - необязательное, атрибуты, которые будут перекрывать атрибуты при наведении,
 *  paths - массив атрибутов path
 *  polygons - массив атрибутов polygon
 */
var RussianMap = function(options, regions, regionsWithRating) {
  var mapId = options.mapId;
  var width = options.width;
  var height = options.height;
  var viewPort = options.viewPort || "0 0 1134 620";
  var viewSize = options.viewSize || "700 700";

  // канва для рисования регионов
  var R = Raphael(mapId, width, height, viewPort, viewSize);

  // дефолтовые атрибуты для контуров регионов
  var defaultAttr = ({ ident }) => {
    // console.log("regionIndex", -(ident - 92), "#" + hsv2rgb(ident, 100, 50));
    const rating = regionsWithRating[ident] ? regionsWithRating[ident].rating : ident;
    if(rating === ident)
    console.log('rating', rating, Math.floor(rating * 10));

    return {
      fill: "rgb(" + hsv2rgb(92, 0, Math.floor(rating * 10)).join(",") + ")", // цвет которым закрашивать
      stroke: "#ffffff", // цвет границы
      "stroke-width": 1, // ширина границы
      "stroke-linejoin": "round" // скруглять углы
    };
  };

  // атрибуты для контуров регионов при событии onMouseMove
  var mouseMoveAttr = ({ ident }) => ({
    fill: "rgb(" + hsv2rgb(55, 100, 90).join(",") + ")"
  });

  /**
   * Событие onMouseMove для path или polygon внутри контекста полигона (можно использовать this).
   */
  var onMouseMove = function(event) {
    var region = this.region;
    // установить всем полигонам в регионе дефолтовые атрибуты
    if (region.paths !== undefined) {
      for (var p in region.paths) {
        region.paths[p].attr(region.moveAttr || mouseMoveAttr(region));
      }
    }
    if (region.polygons !== undefined) {
      for (var p in region.polygons) {
        region.polygons[p].attr(region.moveAttr || mouseMoveAttr(region));
      }
    }
    if (options.onMouseMove) {
      options.onMouseMove.call(this, event);
    }
  };

  /**
   * Событие при клике на полигон
   */
  var onMouseClick = function(event) {
    if (options.onMouseClick) {
      var region = this.region;
      options.onMouseClick.call(this, event);
    }
  };

  /**
   * Событие onMouseOut для path или polygon внутри контекста полигона (можно использовать this)
   */
  var onMouseOut = function(event) {
    var region = this.region;
    // установить всем полигонам в регионе дефолтовые атрибуты
    if (region.paths !== undefined) {
      for (var p in region.paths) {
        region.paths[p].attr(region.attr || defaultAttr(region));
      }
    }
    if (region.polygons !== undefined) {
      for (var p in region.polygons) {
        region.polygons[p].attr(region.attr || defaultAttr(region));
      }
    }
    if (options.onMouseOut) {
      options.onMouseOut.call(this, event);
    }
  };

  /**
   * Рендер региона
   * @param R
   * @param region
   */
  var renderRegion = function(region) {
    // прорисовка многоугольников
    if (region.paths !== undefined) {
      for (var p in region.paths) {
        var path = R.path(region.paths[p]).attr(
          region.attr || defaultAttr(region)
        );
        path.region = region;
        path.mouseover(onMouseMove);
        path.mouseout(onMouseOut);
        path.click(onMouseClick);
        region.paths[p] = path;
      }
    }
    // прорисовка полигнов: также через тег path, только конечная точка соединяется с начальной
    if (region.polygons !== undefined) {
      for (var p in region.polygons) {
        var polygon = R.path("M" + region.polygons[p]).attr(
          region.attr || defaultAttr(region)
        );
        polygon.region = region;
        polygon.mouseover(onMouseMove);
        polygon.mouseout(onMouseOut);
        polygon.click(onMouseClick);
        region.polygons[p] = polygon;
      }
    }
  };

  for (var i in regions) {
    renderRegion(regions[i]);
  }

  this.regions = regions;
};

function hsv2rgb(h, s, v) {
  var r, g, b, i, f, p, q, t;

  h /= 360;
  s /= 100;
  v /= 100;

  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);

  switch (i) {
    case 0:
      r = v;
      g = t;
      b = p;
      break;
    case 1:
      r = q;
      g = v;
      b = p;
      break;
    case 2:
      r = p;
      g = v;
      b = t;
      break;
    case 3:
      r = p;
      g = q;
      b = v;
      break;
    case 4:
      r = t;
      g = p;
      b = v;
      break;
    case 5:
      r = v;
      g = p;
      b = q;
      break;
  }

  var rgb = [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
  return rgb;
}


 Vue.component('person', {
    data: function () {
      return {
        hovered: false,
        chart: {},
        chart2: {},
      }
    },
    computed: {
      chartId(){
        return 'chaaa';
      },
      styleObj() {
        return {
          'background-color': 'rgb(' + hsv2rgb(92, 0, Math.floor((10-this.info.rating) * 10)).join(',') + ')',
          'color': 'rgb(' + hsv2rgb(92, 0, this.info.rating < 5? 0 : 100).join(',') + ')'
        }
      },
      labes() {
        return Object.keys(this.info.personal_income);
      }
    },
    watch:{
      info() {
        console.log('upd');
        // this.chart.update();
        // this.chart2.update();
        this.charts();
      }
    },
    methods:{
      charts() {
const ctx = document.getElementById(this.chartId).getContext('2d');
      this.chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data:  {
        labels: Object.keys(this.info.personal_income),
        datasets: [{
        fill:false,
          label: "Доход",
          // backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 235, 0)',
          data: Object.values(this.info.personal_income),
        },
        ],
      },

        // Configuration options go here
        options: {
          animation: {
            duration: 500,
          },
        },
      });
      const ctx2 = document.getElementById(this.chartId + 2).getContext('2d');

       this.chart2 = new Chart(ctx2, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data:  {
        labels: Object.keys(this.info.personal_income),
        datasets: [{
        fill:false,

          label: "Недвижимость",
          // backgroundColor: 'rgb(155, 99, 132)',
          borderColor: 'rgb(100, 100, 100)',
          data: Object.values(this.info.real_estate_amount),
        }],
      },

        // Configuration options go here
        options: {
          animation: {
            duration: 500,
          },
        },
      });
      }
    },
    mounted() {
      setTimeout(() => {
        this.charts();
      }, 100);
    },
    props: ['name', 'info'],
    template: `<div>
    <div class="wrap">
      <div>
        <div class="wrap">
          <div class="yellow heading person-name d-block">{{name}}<br><small class="pos"> {{info.last_position}}</small></div> 
          <div class="mut" v-bind:style="styleObj" @mouseover="hovered = ! hovered" @mouseout="hovered = false">{{info.rating?info.rating.toFixed(2):''}}</div>
        </div> 
        <div v-for="(item, key) in info.risk_factors">
            <yellow-block v-bind:ttt="item"></yellow-block>
        </div>
         <!--<h2  class="pa"> Количество недвижимости </h2>
        <div v-for="(item, key) in info.real_estate_amount">
          <yellow-block v-bind:ttt="key + ': ' + item"></yellow-block>
        </div> -->
      </div>
      <div>
        <div class="mut-info" v-if="hovered">
            <h3>Индекс мутности</h3>
            Число найденных аномалий в статистических данных для данного служащего. Аномалии могут включать в себя странное соотношения стоимости недвижимости и годового дохода чиновника, имущество его родственников, знакомство с людьми, замешанными в коррупции и т.д.
        </div>
        <h2 v-if="info.affiliates && info.affiliates.length > 0" class="pa"> Аффилирован </h2>
        <div v-for="(item, key) in info.affiliates">
          <yellow-block-aff v-bind:ttt="item"></yellow-block-aff>
        </div>
 <!--
        <h2  class="pa"> Доход </h2>
         <div v-for="(item, key) in info.personal_income">
           <yellow-block v-bind:ttt="key + ': ' + item+' руб.'"></yellow-block>
         </div> -->
      </div>
     
    </div>
    <div class="wrap ">
      <div class="chart">
          <canvas :id="chartId" height="100" width="150">
          </canvas>
        </div>
        <div class="chart">
          <canvas :id="chartId+ 2" height="100" width="150">
          </canvas>
        </div>
      </div>
      </div>
      `
  });